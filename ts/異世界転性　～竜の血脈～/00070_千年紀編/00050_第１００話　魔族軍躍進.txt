在魔族軍隊實質控制魔法都市後，他們的下一個目標是神聖都市。

神聖都市巴蘭。
至今仍信仰著那些當年存在於這世上的神，傳授其教義並教導神聖魔法的都市。
而這座城是理所當然不可能與魔族共存的。一場無法避免的戰爭就在眼前。

一百萬的魔族大軍，幾乎全軍都朝著攻克這座城市而前進著。
魔法都市當局在投降後因為念及舊情而派人去神聖都市勸降，但被送回來的只有交涉人的首級。

「人類似乎在不知不覺間變的比我們想像中還要野蠻阿」

零一邊說著一邊下令讓軍隊進行總攻擊。

巴蘭的城牆跟魔法都市不同，足足有２０公尺高，而且各種防衛設施都非常齊全。
即使是巨人族的攻擊也靠著魔法防禦住了，第一天的攻勢以魔族軍單方面受損結束。

但還來不及迎接第二天的清晨，狀況就發生了。
指揮各個部隊的聖騎士們遭到大量的暗殺。
完全不清楚犯人是誰，也完全沒有目擊者。儘管當下還是深夜，神殿高層迫於現實必須儘早決定各部隊繼任的人選。
然後此時發生了第二個狀況。
那就是聚集起來的高層們也被暗殺了。

守軍在疑神疑鬼的情況下迎接第二天的戰鬥。
這天魔族軍的攻擊比第一天緩和，彷彿是在尋找防守上的弱點。
但儘管如此仍造成巴蘭嚴重的損害。
這是因為急忙就任的指揮官們無法好好的下達指示的關係。

然後是充滿恐怖的第二天夜晚降臨了。
這天晚上又有多名高官慘遭殺害，而且這次連神殿祭司都不例外。

在圍城戰中，一旦失去鬥志勝負就決定了。
在這層意義上，巴蘭到第三天中午都還沒有戰敗。
決定性的事件發生在第四天早上，
有著最高層級警戒的最高祭司醒來時發現枕頭旁插著一柄匕首。
這所代表的意義很簡單，那就是「我們隨時都可以殺死你」

正因為如此，巴蘭的高層決定投降。

但是前線士兵拒絶投降。
因為雖然他們有遭受損失，但城牆仍舊存在著，而且還有許多能繼任指揮官一職的人選。

但就算是他們也在第五天喪失戰意。
到昨天為止負責指揮的聖騎士們大量的被暗殺了。
而且到最後都沒辦法找到暗殺者的蛛絲馬跡。
那是因為經過這座城市的行商人們跟事兵們都很熟識，讓士兵們而完全沒有懷疑到他們身上。

巴蘭被要求解除武裝，而一起在這戰鬥的魔法都市士兵們則被允許歸國。
聖騎士們全部都被隔離開來，暫時性的剝奪了他們的自由。
於是乎人們自誇的神聖都市巴蘭，僅僅六天就落入了魔族的手中。

不過零並沒有讓魔族軍隊進城，因為她很了解市民的情感。
她所做的處置是破壊城牆，並讓城門完全開啟。
她也完全沒有讓魔族軍碰觸那些祭祀眾神的神殿。
而巴蘭的居民的如今只能心驚膽跳的住在沒有城牆的都市裡。

魔族軍的進攻並沒有到此就停止。
約半數戰鬥力較弱的部隊被留下來維持治安，而剩下的精銳部隊則朝著大陸中心，也就人類帝國曾經所在的地方進軍。
而現在那塊區域裡有著為了恢復秩序而出兵的卡薩莉雅與雷姆多利亞兩國的軍隊。
而這裡將會是魔族軍與卡薩莉雅、雷姆多利亞聯軍決戰的場所吧。

───

另一方面在大陸東方，

這裡在某種意義上比奧古斯還要平靜。
首先是大森林，由於魔王以前曾經直接下過命令的關係，因此魔族完全沒有對其出手。
就跟人類與精靈所保持的關係那樣，魔族與精靈雙方也保持彼此互不干涉的關係。

而同樣在大陸東方的伊斯特利亞，因為長久的內戰如今沒有餘力與魔族軍一戰了。
伊斯特利亞長達十年的內戰結束了，而新登基的國王昭示了解放奴隷這項新政策。
而這裡所謂的奴隷自然也包括亞人族在內，正因為這點讓伊斯特利亞與魔族間能夠進行協商。
而實際上伊斯特利亞反而因為引進了魔族的耕作法，得以讓廣大荒蕪的土地迅速復甦，不過這是後話了。
總之伊斯特利亞的新王承認魔族移民，等於是實質與魔族結盟，與奧古斯也建立了差不多的關係。

───

大陸南方的國家一開始是置身於事外的。

但是魔法都市與神聖都市相繼陷落，讓他們回想起有關於千年記的傳說。
那就是除了帝國首都與大陸東方外海的群島外，其他區域的人類全部都被抹殺了的這件事。
大陸西南方的魯阿布拉如今只是個空有其名的國家，而負責保護這個國家名字的大部落族長，決定向人類方派遣援軍。
大陸南方的卡拉斯利因為商人造成的動盪，如今處於無法調動軍隊的窘境，這正是魔王長久以來在此進行的策略，只是人類方沒人知道這點。
大陸東南方的七城聯盟，雖然長久以來不斷的與雷姆多利亞交戰，但最後仍決定出兵支援。但如今他們已難以組成大軍，幾乎都是擔任後勤支援為主。

就這樣雙方的準備一決勝負了。人類方由卡薩莉雅及雷姆多利亞各出兵５０萬，西南大陸的援軍１０萬，然後中小國家各自出兵湊一湊約１０萬，合計兵力大約１２０萬。
這樣的軍隊毫無疑問是人類史上最多也最龐大的。

總司令由雷姆多利亞的王子琉庫西昂王子擔任。雖然年齡才２１歳非常的年輕，但因為他曾經攻陷了他父王琉庫霍林都無法攻陷的城市，因此對於戰爭非常上手。
但也因為他的戰鬥直覺，因此這支大軍並沒有進行移動。
他的作戰計畫非常簡單，那就是以雷姆多利亞軍作為正面迎戰魔族軍，而卡薩莉雅與其他國家的兵力則進行側面包圍。
而他選定了帝國領的北方一塊叫作法魯薩斯的地方來進行決戰。

───

另一方面位於雷姆多利亞首都的黑貓總部，召開了已經不知道是第幾回的會議。

「這次戰爭我們就旁觀吧」

一直都是最後才發言的大和 ── 聖帝琉庫西法卡如此說著。

「這樣做好嗎？」

托爾基於各種意意開口問著。
西法卡點了點頭。

「我們一旦出手戰鬥的話，只要阿魯斯有那個在，結果還是會一樣吧。而且這樣一來反而會給在場的兩支軍隊帶來毀滅性的打擊吧」
「那麼，已經放棄阻止大崩壊了嗎？」

此時平常都沒說話的阿爾維斯提出了疑問。

───

他在１２００年前率領著竜翼大陸上殘存的人類來到這座竜骨大陸，同時也是另一名大賢者。

「並不是放棄，只是現階段沒有辦法出手」

但西法卡的眼神似乎已經什麼都不在乎了。

「如果只是眼下這場戰鬥的話，靠我的爆裂魔法就能幫人類側打贏了說⋯」

平常很活潑的夏娜如今處於這個狀態也變的沒有精神了。

「這樣做沒有意義。就算打倒了眼前的５０萬魔族，後面還留有另外５０萬魔族阿。而且⋯⋯」

西法卡抱著手繼續陳述事實。

「⋯在魔族本國裡，恐怕還保有１０倍以上的兵力」

───

這是事實。

總計一千萬的魔族軍隊。但是將魔族內擅長戰鬥的種族佔大多數這點計算進去的話，敵人的戰力遠不只如此。
這場戰爭在失去黃金竜庫拉利斯的當下，就等於是決定了人類側敗北的命運。

「不能找巴魯斯幫忙嗎？以前她也曾經站在人類陣營一起戰鬥過吧」

雖然已經大致明白現狀了，但亞傑魯仍忍不住發問。

「在失去庫拉利斯的現在，萬一連巴魯斯都失去的話，我們會撐不過大崩壊的，因此現狀是不可能向她求援的」

由於這個言論是出自唯一跨越過大崩壊的人之口，所以沒有人能反駁。

───

「結果連個決定都做不出來阿」

托爾嘆了一口氣說著。
不過對此西法卡否定了托爾的結論。

「我打算跟琉庫蕾雅娜女王見一面」

這對於平常幾乎沒有任何行動的西法卡來說是非常罕見的事。

「亞傑魯你也一起來。可以的話要將屠竜者卡拉也拉到我們這邊來」

是的，西法卡還沒放棄。
恐怕與魔王並列的聖帝，是這個世界上最不懂得放棄的人吧。

「另外也得跟庫歐談談。眼下這個事態恐怕大森林也沒辦法毫髮無傷吧」

那雙衰老的眼睛中確實閃耀著光芒，一一確認同伴的回應。